// O models estará responsável pela criaçãode todas as classes que
//fará ligação com o Banco de Dados(Msql)

const users = []; //aqui estaria o banco de dados, aqui vai quardada
//os usuarios cadastrados

class User { //a classe usuário
  constructor(name, email) {
    this.name = name;
    this.email = email;
  }

  static addUser(user) { //metódo envia o usuario pro banco
    users.push(user);
  }

  static getAllUsers() {//metódo listar usuário
    return users;
  }
}

module.exports = User; // a chave para exportação do arquivo