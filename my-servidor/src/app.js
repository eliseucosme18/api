const express = require('express');
const app = express();
const port = 3000;
const userRoutes = require('./routes/userRoutes'); //está iimportando o módulo, permitindo usar as funções do arquivo router(js)

// Middleware para processar JSON
app.use(express.json());

// Rota inicial
app.get('/', (req, res) => {
  res.send('Bem-vindo à API de tarefas!');
});
// Rota para as tarefas
app.get('/adicionar', userRoutes);
app.get('/listar', userRoutes);

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});